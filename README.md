These are the programming assignments relative to Machine Learning - Clustering and Retrieval (4th part):

* Assignment 2: implementing Locality Sensitive Hashing
* Assignment 3: clustering Wikipedia documents using k-means
* Assignment 4: implementing the EM algorithm for a Gaussian mixture model
* Assignment 5: applying standard preprocessing techniques on Wikipedia text data and fitting a Latent Dirichlet allocation (LDA) model
* Assignment 6: exploring a top-down approach, recursively bipartitioning the data using k-means

For more information, I invite you to have a look at https://www.coursera.org/learn/ml-clustering-and-retrieval
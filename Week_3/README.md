Clustering text data with k-means

In this assignment you will

Cluster Wikipedia documents using k-means
Explore the role of random initialization on the quality of the clustering
Explore how results differ after changing the number of clusters
Evaluate clustering, both quantitatively and qualitatively
When properly executed, clustering uncovers valuable insights from a set of unlabeled documents.

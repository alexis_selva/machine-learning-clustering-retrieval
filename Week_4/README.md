Implementing EM for Gaussian mixtures

In this assignment you will

implement the EM algorithm for a Gaussian mixture model
apply your implementation to cluster images
explore clustering results and interpret the output of the EM algorithm


Clustering text data with Gaussian mixtures

In a previous assignment, we explored K-means clustering for a high-dimensional Wikipedia dataset. We can also model this data with a mixture of Gaussians, though with increasing dimension we run into several important problems associated with using a full covariance matrix for each component.

In this section, we will use an EM implementation to fit a Gaussian mixture model with diagonal covariances to a subset of the Wikipedia dataset.
